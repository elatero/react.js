import React from 'react'
import styles from './MenuToggle.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faBars } from '@fortawesome/free-solid-svg-icons'

const MenuToggle = props => {
  const cls = [
    styles.MenuToggle
  ]
  const icon = []

  if (props.isOpen) {
    icon.push(faTimes)
    cls.push(styles.open)
  } else {
    icon.push(faBars)
  }

  return (
    <FontAwesomeIcon 
      icon = {icon[0]}
      className = {cls.join(' ')}
      onClick = {props.onToggle}
    />
  )
}

export default MenuToggle