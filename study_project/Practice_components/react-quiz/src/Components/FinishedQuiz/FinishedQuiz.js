import React from 'react'
import styles from './FinishedQuiz.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import Button from '../UI/Button/Button'

const FinishedQuiz = props => {

  const successCount = Object.keys(props.results).reduce((total, key) => {
    if (props.results[key] === 'success') {
      total++
    }
    return total
  }, 0)

  return (
    <div className={styles.FinishedQuiz}>
      <ul>
        { props.quiz.map((quizItem, index) => {
          const cls =  [
            props.results[quizItem.id] === 'error' ? faTimes : faCheck,
            styles[props.results[quizItem.id]]
          ]

          return (
            <li key={index}>
              <strong>{ index + 1 }. </strong>&nbsp;
              {quizItem.question}
              <FontAwesomeIcon icon={cls[0]} className={cls[1]}/>
            </li>
          )
        }) }        
      </ul>
      <p>Right { successCount } of { props.quiz.length }</p>
      <div>        
        <Button onClick = {props.onRetry} type="primary">Repeat</Button>
        <Button type="success">Go to test list</Button>
      </div>
    </div>
  )
}

export default FinishedQuiz