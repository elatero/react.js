import React from 'react';
import logo from './logo.svg';
import './App.css';
import Car from './Components/Car'

class App extends React.Component {
  
  state = {
    cars: [
      { id: 1, name: 'Ford', year: 2018 },
      { id: 2, name: 'Audi', year: 2016 },
      { id: 3, name: 'Mazda', year: 2010 },
    ],
    pageTitle: 'React components',
    showCars: false
  }

  changeTitleHandler = (newTitle) => {

    this.setState({
      pageTitle: newTitle
    })

  }

  handleInput  = (event) => {
    this.setState ({
      pageTitle: event.target.value
    })
  }

  toggleCarsHandler = () => {
    this.setState({
      showCars: !this.state.showCars
    })
  }

  onChangeName(name, index) {    
    const car = this.state.cars[index]
    car.name = name
    const cars = [...this.state.cars]
    cars[index] = car
    this.setState({
      cars
    })
  }

  deleteHandler = (index) => {
    const cars = [...this.state.cars]
    
    cars.splice(index, 1)

    this.setState({
      cars
    })
  }

  render () {

    let cars = null

    if (this.state.showCars) {
      cars = this.state.cars.map((car, index) => {
              return(
                <Car
                  name = { car.name }
                  year = { car.year }
                  onChangeTitle = {() => this.changeTitleHandler(car.name)}
                  onChangeName = {(event) => this.onChangeName(event.target.value, index)}
                  onDelete = {(index) => this.deleteHandler(index)}
                  key = { index }
                />
              )
            })
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <div style={{marginBottom: '20px'}}>
          <h3 style={{textAlign: 'center', color: 'red', fontSize: '24px', padding: '10px'}}>{this.state.pageTitle}</h3>

          <input type="text" onChange={this.handleInput}/>

          <button onClick={this.changeTitleHandler.bind(this, 'Changed!')}>Change title</button>

          <button onClick={this.toggleCarsHandler}>Toggle cars</button>

          <div>{ cars }</div>
        </div>
      </div>
      );
    }
  } 

export default App;
