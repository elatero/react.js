import React from 'react'
import styles from'./Car.module.css'
import PropTypes from 'prop-types'
// import Radium from 'radium'

class Car extends React.Component {

  constructor (props) {
    super(props)

    this.inputRef = React.createRef()
  }
  
  componentDidMount () {
    if (this.props.index === 0) {
      this.inputRef.current.focus()
    }
  }

  componentWillReceiveProps (nextProps) {
    console.log('Car componentWillReceiveProps', nextProps)
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    console.log('Car shouldComponentUpdate', nextProps, nextState, nextContext)
    return true
  }

  componentWillUpdate (nextProps, nextState) {
    console.log('Car componentWillUpdate', nextProps, nextState)
  }

  static getDerivedStateFromProps (nextProps, prevState) {
    console.log('Car getDerivedStateFromProps', nextProps, prevState)

    return prevState
  }

  componentDidUpdate () {
    console.log('Car componentDidUpdate')
  }

  getSnapshotBeforeUpdate () {
    console.log('Car getSnapshotBeforeUpdate')
  }

  componentWillUnmount () {
    console.log('componentWillUnmount')
  }
  render () {

    console.log('Car render')

    // if (Math.random () > 0.7) {
    //   throw new Error('Car random failed')
    // }

    const inputClasses = [styles.input]

  if (this.props.name !== '') {
    inputClasses.push(styles.green)
  } else {
    inputClasses.push(styles.red)
  }

  if (this.props.name.length > 4) {
    inputClasses.push(styles.bold)
  }

  const style = {
    border: '1px solid #ccc',
    boxShadow: '0 4px 5px 0 rgba(0, 0, 0, .14)',
    ':hover': {
      border: '1px solid #aaa',
      boxShadow: '0 4px 15px 0 rgba(0, 0, 0, .25)',
      cursor: 'pointer'
    }
  }

  return (
      <div className={styles.Car} style={style}>
        <h2>Car name: {this.props.name}</h2>
        <p>Year: <strong>{ this.props.year }</strong></p>
        <input
          ref={this.inputRef}
          type="text"
          onChange={this.props.onChangeName}
          value={this.props.name}
          className={inputClasses.join(' ')}
        />
        <button onClick={this.props.onChangeTitle}>Click</button>
        <button onClick={this.props.onDelete}>Delete</button>
      </div>
    )
  }
}

Car.propTypes = {
  name: PropTypes.string.isRequired,
  year: PropTypes.number,
  index: PropTypes.number,
  onCahgr: PropTypes.func,
  onDelete: PropTypes.func
}

export default Car