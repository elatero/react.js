import React from 'react';
import logo from './logo.svg';
import './App.css';
import Car from './Components/Car'
import ErrorBoundary from './Components/ErrorBoundary/ErrorBoundary'
import Counter from './Components/Counter/counter'
import styles from './App.module.sass'

export const ClickedContext = React.createContext(false)

class App extends React.Component {

  constructor (props) {
    console.log('App constructor')
    super(props)

    this.state = {
      clicked: false,
      cars: [
        { id: 1, name: 'Ford', year: 2018 },
        { id: 2, name: 'Audi', year: 2016 },
        { id: 3, name: 'Mazda', year: 2010 },
      ],
      pageTitle: 'React components',
      showCars: false
    }
  }

  changeTitleHandler = (newTitle) => {

    this.setState({
      pageTitle: newTitle
    })

  }

  handleInput  = (event) => {
    this.setState ({
      pageTitle: event.target.value
    })
  }

  toggleCarsHandler = () => {
    this.setState({
      showCars: !this.state.showCars
    })
  }

  onChangeName(name, index) {    
    const car = this.state.cars[index]
    car.name = name
    const cars = [...this.state.cars]
    cars[index] = car
    this.setState({
      cars
    })
  }

  deleteHandler = (index) => {
    const cars = [...this.state.cars]
    
    cars.splice(index, 1)

    this.setState({
      cars
    })
  }

  componentWillMount () {
    console.log('App componentWillMount')
  }

  componentDidMount () {
    console.log('App componentDidMount')
  }  

  render () {

    console.log('App render')

    let cars = null

    if (this.state.showCars) {
      cars = this.state.cars.map((car, index) => {
              return(
                <ErrorBoundary  key = { index }>
                  <Car
                    index = { index }
                    name = { car.name }
                    year = { car.year }
                    onChangeTitle = {() => this.changeTitleHandler(car.name)}
                    onChangeName = {(event) => this.onChangeName(event.target.value, index)}
                    onDelete = {(index) => this.deleteHandler(index)}                   
                  />
                </ErrorBoundary>  
              )
            })
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <div style={{marginBottom: '20px'}}>
          <h3 style={{textAlign: 'center', color: 'red', fontSize: '24px', padding: '10px'}}>{this.state.pageTitle}</h3>
          <h5>{this.props.title}</h5>

          <ClickedContext.Provider value={this.state.clicked}>
            <Counter />
          </ClickedContext.Provider>
          

          <hr/>

          <button
            style={{marginTop: '20px'}}
            onClick={this.toggleCarsHandler}
            className={styles.AppButton}
          >Toggle cars</button>

          <button onClick={() => this.setState({clicked: true})}>Change clicked</button>

          <div style={{
            padding: '10px',
            width: '400px',
            margin: 'auto'
          }}>{ cars }</div>
        </div>
      </div>
      );
    }
  } 

export default App;
