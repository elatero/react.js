import React from 'react'

// function Car() {
//   return (
//     <h2>This is car component</h2>
//   )
// }

// const Car = () => {
//   return (
//     <h2>This is car component</h2>
//   )
// }

// const Car = () => (
//   <div>
//     <h2>This is car component</h2>
//     <strong>test</strong>
//   </div>
// )

export default  (props) => (
  <div>
    <h2>Car name: {props.name}</h2>
    <p>Year: <strong>{ props.year }</strong></p>
    { props.children }
  </div>
)