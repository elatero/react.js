import React from 'react';
import logo from './logo.svg';
import './App.css';
import Car from './Components/Car'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
      {/* inline style */}

      <div style={{textAlign: 'center', color: 'red', fontSize: '24px', padding: '10px'}}>Hello World!</div>

      {/* Componets and props */}

      <Car name={'Ford'} year={2018}>
        <p style={{color: 'green'}}>Color</p>
      </Car>
      <Car name="Audi" year={2016} />
      <Car name={'Mazda'} year={2010} />
    </div>
  );
}

export default App;
