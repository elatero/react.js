import axios from 'axios'

export default  axios.create({
  baseURL: 'https://reactquiz-73a8d-1407e.firebaseio.com'
})