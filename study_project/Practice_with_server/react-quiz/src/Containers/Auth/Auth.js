import React, { Component } from 'react'
import styles from './Auth.module.css'
import Button from '../../Components/UI/Button/Button'
import Input from '../../Components/UI/Input/Input'
import isJs from 'is_js'
import axios from 'axios'

export default class Auth extends Component {

  state = {
    isFormValide: false,
    formControls: {
      email: {
        value: '',
        type: 'email',
        label: 'Email',
        errorMassage: 'Enter the correct email!',
        valid: false,
        touched: false,
        validation : {
          requred: true,
          email: true
        }
      },
      password: {
        value: '',
        type: 'password',
        label: 'Password',
        errorMassage: 'Enter the correct password!',
        valid: false,
        touched: false,
        validation : {
          requred: true,
          minlength: 6
        }
      }
    }
  }

  loginHandler = async () => {

    const authData = {
      email: this.state.formControls.email.value,
      password: this.state.formControls.password.value,
      returnSecureToken: true
    }

    try {
      const response = await axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAgW9Gim-NWK-H3tLxT-Kb4NInYtsrqJ1c', authData)

      console.log(response.data)
    } catch (error) {
      
    }

  }

  registerHandler = async () => {

    const authData = {
      email: this.state.formControls.email.value,
      password: this.state.formControls.password.value,
      returnSecureToken: true
    }

    try {
      const response = await axios.post('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAgW9Gim-NWK-H3tLxT-Kb4NInYtsrqJ1c', authData)

      console.log(response.data)
    } catch (error) {
      
    }

  }

  submitHandler = event => {
    event.preventDefault()
  }

  validateControl(value, validation) {
    if (!validation) {
      return true
    }

    let isValid = true 

    if (validation.requred) {
      isValid = value.trim() !== '' && isValid
    }

    if (validation.email) {
      isValid = isJs.email(value) && isValid
    }

    if (validation.minlength) {
      isValid = value.length >= validation.minlength && isValid
    }

    return isValid
  }

  onChangeHandler = (event, controlName) => {
    const formControls = { ...this.state.formControls }
    const control = { ...formControls[controlName] }

    control.value = event.target.value
    control.touched = true
    control.valid = this.validateControl(control.value, control.validation)

    formControls[controlName] = control

    let isFormValid = true

    Object.keys(formControls).forEach(name => {
      isFormValid = formControls[name].valid
    })

    this.setState({
      formControls,
      isFormValid
    })
  }

  renderInputs() {
    return Object.keys(this.state.formControls).map((controlName, index) => {
      const control = this.state.formControls[controlName]
      return (
        <Input
          key={controlName + index}
          type={control.type}
          value={control.value}
          valid={control.valid}
          touched={control.touched}
          label={control.label}
          shouldValidate={!!control.validation}
          errorMassage={control.errorMassage}
          onChange={event => this.onChangeHandler(event, controlName)}
        />
      )
    })
  }

  render() {
    return (
      <div className={styles.Auth}>
        <div>
          <h1>Authorization</h1>
          <form onSubmit={this.submitHandler} className={styles.AuthForm}>
            { this.renderInputs() }
            <Button
              type="success"
              onClick={this.loginHandler}
              disabled={!this.state.isFormValid}
            >Log In</Button>
            <Button
              type="primary"
              onClick={this.registerHandler}
              disabled={!this.state.isFormValid}
            >Registration</Button>
          </form>
        </div>
      </div>
    )
  }
}
