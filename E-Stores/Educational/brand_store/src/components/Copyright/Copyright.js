import React from 'react'
import styles from './Copyright.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { 
  faFacebookF, 
  faTwitter,
  faLinkedinIn,
  faPinterestP,
  faGooglePlusG
} from '@fortawesome/free-brands-svg-icons'

const Copyright = () => {
  return (
    <div className={styles.copyright}>
      <div className={styles.copyright__container}>
        <p className={styles.copyrightText}>© 2017  Brand  All Rights Reserved.</p>
        <div className={styles.socialBox}>
          <a href="##" className={styles.socialIcon}>
            <FontAwesomeIcon icon={faFacebookF} />
          </a>
          <a href="##" className={styles.socialIcon}>
            <FontAwesomeIcon icon={faTwitter} />
          </a>
          <a href="##" className={styles.socialIcon_noLink}>
            <FontAwesomeIcon icon={faLinkedinIn} />
          </a>
          <a href="##" className={styles.socialIcon}>
            <FontAwesomeIcon icon={faPinterestP} />
          </a>
          <a href="##" className={styles.socialIcon}>
            <FontAwesomeIcon icon={faGooglePlusG} />
          </a>
        </div>
      </div>    
    </div>
  )
}

export default Copyright
