import React from 'react'
import styles from './Header.module.sass'
import Logo from '../UI/Logo'
import SearchForm from '../UI/SearchForm'
import Cart from '../UI/Cart'
import BtnAccount from '../UI/BtnAccount'

const Header = () => {
  return (
    <header className={styles.header}>
      <div className={styles.container}>
        <Logo />
        <div className={styles.SearchForm}>
          <SearchForm />
        </div>
        <div className={styles.cartBox}>
          <div className={styles.cartBox__cart}>
            <Cart />
          </div>          
          <BtnAccount />
        </div>
      </div>
    </header>
  )
}

export default Header