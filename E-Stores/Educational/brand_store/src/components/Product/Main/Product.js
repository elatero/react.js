import  React from 'react'
import { Link } from 'react-router-dom'

import styles from './Product.module.sass'
import Cart from '../img/cart.png'

const Product = (props) => {
  return (
    <Link to="singlePage">
      <div className={styles.productBox}>
        <div className={styles.product}>
          <img src={props.img} alt={props.name} className={styles.product__img}/>
          <div className={styles.product_description}>
            <h3 className={styles.product_description__name}>
              {props.name}
            </h3>
            <p className={styles.product_description__price}>
              ${props.price}
            </p>          
          </div>
        </div>
        <div className={styles.boxBtn}>
          <button 
            className={styles.buyBtn} 
            id={props.id}
            onClick = {props.buy}
          >
            <img src={Cart} alt="cartIcon" className={styles.buyBtn__img}/>
            Add to Cart
          </button>
        </div>
      </div>
    </Link>
  )
}

export default Product

