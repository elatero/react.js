import  React from 'react'
import { NavLink } from 'react-router-dom'

import styles from './Product.module.sass'
import Cart from '../img/cart.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRetweet, faHeart } from '@fortawesome/free-solid-svg-icons'

const Product = (props) => {
  return (
    <NavLink to="singlePage">
      <div className={styles.productBox}>
        <div className={styles.product}>
          <img src={props.img} alt={props.name} className={styles.product__img}/>
          <div className={styles.product_description}>
            <h3 className={styles.product_description__name}>
              {props.name}
            </h3>
            <p className={styles.product_description__price}>
              ${props.price}
            </p>          
          </div>
        </div>
        <div className={styles.boxBtn}>
          <div>
            <button 
              className={styles.buyBtn} 
              id={props.id}
              onClick = {props.buy}
            >
              <img src={Cart} alt="cartIcon" className={styles.buyBtn__img}/>
              Add to Cart
            </button>
          </div>
          <div>
            <button className={styles.retweetBtn}>
              <FontAwesomeIcon icon={faRetweet} className={styles.retweetBtn__img}/>
            </button>
            <button className={styles.likeBtn}>
              <FontAwesomeIcon icon={faHeart}  className={styles.likeBtn__img}/>
            </button>
          </div>
        </div>
      </div>
    </NavLink>
  )
}

export default Product

