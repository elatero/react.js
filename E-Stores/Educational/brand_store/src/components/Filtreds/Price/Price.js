import React from 'react'
import { Range } from 'rc-slider'
import 'rc-slider/assets/index.css'
import styles from './Price.module.sass'

const style = { width: 283, margin: 0 }

function log(value) {
  console.log(value)
}

class Price extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [52, 400],
    };
  }
  
  onSliderChange = (value) => {
    log(value);
    this.setState({
      value,
    });
  }
  render () {
    let handleStyle = [
      {
        backgroundColor: '#f16d7f',
        border: 'none',
        width: '14px',
        height: '14px'
      },
      {
        backgroundColor: '#f16d7f',
        border: 'none',
        width: '14px',
        height: '14px'
      }
    ]

    let trackStyle = [
      {
        backgroundColor: '#f16d7f',
        height: '6px'        
      }
    ]

    let railStyle = {
      backgroundColor: '#f0f0f0',
      height: '6px',
      borderRadius: '3px'
    }

    return (
      <div className={styles.Price}>
        <h2 className={styles.title}>PRICE</h2>
        <div style={style}>      
          <Range
            allowCross={false} 
            min={0} max={1000} 
            value={this.state.value} 
            onChange={this.onSliderChange} 
            handleStyle={handleStyle} 
            trackStyle={trackStyle} 
            railStyle={railStyle}
            defaultValue={[52, 400]}
          />
          <div className={styles.sliderPrice}>
            <span> {this.state.value[0]}$</span>
            <span> {this.state.value[1]}$</span>
          </div>
        </div>
      </div>
    )
  }
  
}

export default Price