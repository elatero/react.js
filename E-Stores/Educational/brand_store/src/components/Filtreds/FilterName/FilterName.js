import React, { useState } from 'react'
import styles from './FilterName.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretUp, faCaretDown } from '@fortawesome/free-solid-svg-icons'

const FilterName = () => {

  const [category, setVisibleCategory] = useState(false)
  const [brand, setVisibleBrand] = useState(false)
  const [designer, setVisibleDisigner] = useState(false)
  
  return (
    <div className={styles.FilterName}>
      <div className={styles.category} onClick = { () => setVisibleCategory(!category)}>
        <h3 className={styles.title}>Category</h3>
        <FontAwesomeIcon 
          icon={category ? faCaretUp : faCaretDown}
          className={category ? styles.iconOpen : styles.iconClose}/>
      </div>
      <nav className={category ? styles.navigation_open : styles.navigation}>
        <ul className={styles.list}>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Accessories</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Bags</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Denim</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Hoodies & Sweatshirts</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Jackets & Coats</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Pants</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Polos</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Shirts</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Shoes</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Shorts</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Sweaters & Knits</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>T-Shirts</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Tanks</a>
          </li>
        </ul>
      </nav>
      <div className={styles.category} onClick = { () => setVisibleBrand(!brand)}>
        <h3 className={styles.title}>BRAND</h3>
        <FontAwesomeIcon 
          icon={brand ? faCaretUp : faCaretDown}
          className={brand ? styles.iconOpen : styles.iconClose}/>
      </div>
      <nav className={brand ? styles.navigation : styles.navigation_open}>
      </nav>
      <div className={styles.category} onClick = { () => setVisibleDisigner(!designer)}>
        <h3 className={styles.title}>DESIGNER</h3>
        <FontAwesomeIcon 
          icon={designer ? faCaretUp : faCaretDown}
          className={designer ? styles.iconOpen : styles.iconClose}/>
      </div>
      <nav className={designer ? styles.navigation : styles.navigation_open}>
      </nav>
    </div>
  )
}

export default FilterName