import React from 'react'
import styles from './Size.module.sass'

const Size = () => {
  return (
    <div className={styles.Size}>
      <h2 className={styles.title}>Size</h2>
      <div className={styles.container}>
        <div className={styles.boxSize}>
          <input type="checkbox" name="XXS" id="XXS"/>
          <label htmlFor="XXS">XXS</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="XS" id="XS"/>
          <label htmlFor="XS">XS</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="S" id="S"/>
          <label htmlFor="S">S</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="M" id="M"/>
          <label htmlFor="M">M</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="L" id="L"/>
          <label htmlFor="L">L</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="XL" id="XL"/>
          <label htmlFor="XL">XL</label>
        </div>
        <div className={styles.boxSize}>
          <input type="checkbox" name="XXL" id="XXL"/>
          <label htmlFor="XXL">XXL</label>
        </div>
      </div>
    </div>
  )
}

export default Size