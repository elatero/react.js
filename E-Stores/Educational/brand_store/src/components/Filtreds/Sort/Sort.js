import React from 'react'
import styles from './Sort.module.sass'

const Sort = () => {
  return (
    <div className={styles.Sort}>
      <div className={styles.sortBy}>
        <button className={styles.title}>Sort By</button>
        <select name="sortBy" id="sortBy" className={styles.selectSortBy}>
          <option value="Name">Name</option>
        </select>
      </div>
      <div className={styles.show}>
        <button className={styles.title}>Show</button>
        <select name="sortBy" id="sortBy" className={styles.selectSortBy}>
          <option value="Name">09</option>
        </select>
      </div>
    </div>
  )
}

export default Sort