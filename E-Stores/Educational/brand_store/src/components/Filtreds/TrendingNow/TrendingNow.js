import React from 'react'
import styles from './TrendingNow.module.sass'

const TrendingNow = () => {
  return (
    <div className={styles.TrendingNow}>
      <h2 className={styles.title}>Trending now</h2>
      <nav className={styles.navigation}>
        <ul className={styles.list}>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Bohemian</a>
          </li>          
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Floral</a>
          </li>          
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Lace</a>
          </li>
        </ul>
        <ul className={styles.list}>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Floral</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Lace</a>
          </li>
          <li className={styles.list__item}>
            <a href="##" className={styles.list__item__link}>Bohemian</a>
          </li>
        </ul>
      </nav>
    </div>
  )
}

export default TrendingNow