import React from 'react';
import styles from './Offer.module.sass'
import OfferImg from './img/offer.jpg'
import DeliveryImg from './img/delivery.png'
import SalesImg from './img/sales.png'
import QualityImg from './img/quality.png'

const Offer = () => {
  return (
    <div className={styles.offer}>
      <div className={styles.offer__container}>
        <div className={styles.offerBox}>
          <img src={OfferImg} alt="offer" className={styles.offerImg}/>
          <div className={styles.boxText}>
            <p className={styles.boxText__title}>30%
              <span className={styles.boxText__title_pink}>OFFER</span></p>
            <p className={styles.boxText__subtitle}>FOR WOMEN</p>
          </div>
          <div className={styles.offerText}>
            <div className={styles.delivery}>
              <img src={DeliveryImg} alt="delivery" className={styles.delivery__img}/>
              <div className={styles.delivery__text}>
                <h3 className={styles.delivery__text__title}>Free Delivery</h3>
                <p className={styles.delivery__text__description}>Worldwide delivery on all.
                  Authorit tively morph
                  next-generation innov
                  tion with extensive models.
                </p>
              </div>
            </div>
            <div className={styles.sales}>
              <img src={SalesImg} alt="sales" className={styles.sales__img}/>
              <div className={styles.sales__text}>
                <h3 className={styles.sales__text__title}>Sales & discounts</h3>
                <p className={styles.sales__text__description}>Worldwide delivery on all.
                  Authorit tively morph
                  next-generation innov
                  tion with extensive models.
                </p>
              </div>
            </div>
            <div className={styles.quality}>
              <img src={QualityImg} alt="qualitity" className={styles.quality__img}/>
              <div className={styles.quality__text}>
                <h3 className={styles.quality__text__title}>Quality assurance</h3>
                <p className={styles.quality__text__description}>Worldwide delivery on all.
                  Authorit tively morph
                  next-generation innov
                  tion with extensive models.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Offer