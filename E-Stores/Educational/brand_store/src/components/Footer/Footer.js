import React from 'react'
import style from './Footer.module.sass'
import Logo from '../UI/Logo'

const Footer = () => {
  return (
    <footer className={style.footer}>
      <div className={style.footer__container}>
        <div className={style.footerDescription}>
          <Logo />
          <p >
            Objectively transition extensive data rather than cross functional 
            solutions. Monotonectally syndicate multidisciplinary materials
            before go forward benefits. Intrinsicly syndicate an expanded
            array of processes and cross-unit partnerships.
          </p>
          <p>
            Efficiently plagiarize 24/365 action items and focused infomediaries.
            Distinctively seize superior initiatives for wireless technologies. 
            Dynamically optimize.
          </p>
        </div>        
        <div className={style.navigations}>
          <nav className={style.navigation}>
            <h2 className={style.navigation__title}>COMPANY</h2>
            <ul className={style.list}>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Home</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Shop</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>About</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>How It Works</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Contact</a>
              </li>
            </ul>
          </nav>
          <nav className={style.navigation}>
            <h2 className={style.navigation__title}>INFORMATION</h2>
            <ul className={style.list}>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Tearms & Condition</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Privacy Policy</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>How to Buy</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>How to Sell</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Promotion</a>
              </li>
            </ul>
          </nav>
          <nav className={style.navigation}>
            <h2 className={style.navigation__title}>SHOP CATEGORY</h2>
            <ul className={style.list}>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Men</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Women</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Child</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Apparel</a>
              </li>
              <li className={style.list__item}>
                <a href="##" className={style.list__item__link}>Brows All</a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </footer>
  )
}

export default Footer