import React from 'react'
import { NavLink } from 'react-router-dom'

import styles from './Navigation.module.sass'
import ImgSale from './img/imgSale.jpg'

const Navigation = () => {

  const dropListMan = () => {

    const title = [ styles.dropListNav__title, styles.dropListNav__title_bottom]

    return (
      <div className={styles.dropListMen}>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Man</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Blazers</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Denim</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Leggings/Pants</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Skirts/Shorts</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Accessories</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Man</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <h2 className={title.join(' ')}>Man</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Man</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <img src={ImgSale} alt="imgSale" className={styles.dropListNav__img} />
        </nav>
      </div>
    )
  }

  const dropLisWomen = () => {

    const title = [ styles.dropListNav__title, styles.dropListNav__title_bottom]

    return (
      <div className={styles.dropListWomen}>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Women</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Blazers</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Denim</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Leggings/Pants</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Skirts/Shorts</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Accessories</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Women</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <h2 className={title.join(' ')}>Women</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Women</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <img src={ImgSale} alt="imgSale" className={styles.dropListNav__img} />
        </nav>
      </div>
    )
  }

  const dropLisKids = () => {

    const title = [ styles.dropListNav__title, styles.dropListNav__title_bottom]

    return (
      <div className={styles.dropListKids}>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Kids</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Blazers</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Denim</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Leggings/Pants</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Skirts/Shorts</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Accessories</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Kids</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <h2 className={title.join(' ')}>Kids</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
          </ul>
        </nav>
        <nav className={styles.dropListNav}>
          <h2 className={styles.dropListNav__title}>Kids</h2>
          <ul className={styles.dropListNav__list}>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Dresses</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Tops</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.dropListNav__list__item}>
              <a href="##" className={styles.dropListNav__list__item__link}>Jackets/Coats</a>
            </li>
          </ul>
          <img src={ImgSale} alt="imgSale" className={styles.dropListNav__img} />
        </nav>
      </div>
    )
  }

  return (
    <nav className={styles.navigation}>
      <div className={styles.container}>
        <ul className={styles.navList}>
          <li className={styles.navList__item}>
            <NavLink 
              to="/"
              exact
              className={styles.navList__item__link}
              activeStyle={{color: '#f16d7f'}}
            >Home</NavLink>
          </li>
          <li className={styles.navList__item}>
            <NavLink
              to="/men"
              className={styles.navList__item__link}
              activeStyle={{color: '#f16d7f'}}
            >Man</NavLink>
            {dropListMan()}
          </li>
          <li className={styles.navList__item}>
            <a href="##" className={styles.navList__item__link}>Women</a>
            {dropLisWomen()}
          </li>
          <li className={styles.navList__item}>
            <a href="##" className={styles.navList__item__link}>Kids</a>
            {dropLisKids()}
          </li>
          <li className={styles.navList__item}>
            <a href="##" className={styles.navList__item__link}>Accoseriese</a>
          </li>
          <li className={styles.navList__item}>
            <a href="##" className={styles.navList__item__link}>Featured</a>
          </li>
          <li className={styles.navList__item}>
            <a href="##" className={styles.navList__item__link}>Hot Deals</a>
          </li>
        </ul>        
      </div>      
    </nav>
  )
}

export default Navigation