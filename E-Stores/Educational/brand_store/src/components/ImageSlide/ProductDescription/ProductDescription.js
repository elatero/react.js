import React from 'react'
import styles from './ProductDescription.module.sass'
import SelectColor from '../../UI/SelectColor'
import Cart from './img/cart.png'

const ProductDescription = () => {
  return (
    <div className={styles.ProductDescription}>
      <div className={styles.boxCaption}>
        <h1 className={styles.caption}>WOMEN COLLECTION</h1>
        <div className={styles.boxLineCaption}>
          <div className={styles.line}></div>
          <div className={styles.lineCenter}></div>
          <div className={styles.line}></div>
        </div>
      </div>
      <h2 className={styles.title}>Moschino Cheap And Chic</h2>
      <p className={styles.description}>
        Compellingly actualize fully researched
        processes before proactive outsourcing.
        Progressively syndicate collaborative
        architectures before cutting-edge services.
        Completely visualize parallel core competencies
        rather than exceptional portals. 
      </p>
      <div className={styles.boxView}>
        <div className={styles.boxView__column}>
          <p className={styles.boxView__column__text}>MATERIAL: <span>COTTON</span></p>
        </div>
        <div className={styles.boxView__column}>
          <p className={styles.boxView__column__text}>DESIGNER: <span>BINBURHAN</span></p>
        </div>
      </div>
      <p className={styles.price}>$561</p>
      <div className={styles.lineGreen}></div>
      <div className={styles.boxParametrs}>
        <div className={styles.boxParametrs__column}>
          <h3 className={styles.boxParametrs__column__title}>CHOOSE COLOR</h3>
          <SelectColor
            Option = {
              [
                { 
                  color: 'red',
                  text: 'Red'
                },
                { 
                  color: 'blue',
                  text: 'Blue'
                },
                {
                  color: 'green',
                  text: 'Green'
                }
              ]
            }  
          />         
        </div>
        <div className={styles.boxParametrs__column}>
          <h3 className={styles.boxParametrs__column__title}>CHOOSE SIZE</h3>
          <select className={styles.sizeSelect}>
            <option value="S">S</option>
            <option value="M">M</option>
            <option value="L">L</option>
            <option value="XL">XL</option>
            <option value="XXL">XXL</option>
          </select>
        </div>
        <div className={styles.boxParametrs__column}>
          <h3 className={styles.boxParametrs__column__title}>QUANTITY</h3>
          <input type="number" placeholder="2" className={styles.inputQuantity}/>
        </div>
      </div>
      <button className={styles.addToCart}>
        <img src={Cart} alt="cart" className={styles.addToCart__img}/>
        Add to Cart
      </button>
    </div>
  )
}

export default ProductDescription