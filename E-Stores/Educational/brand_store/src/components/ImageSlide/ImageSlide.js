import React from 'react'
import Product from './img/product.jpg'
import ProductDescription from './ProductDescription'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight } from '@fortawesome/free-solid-svg-icons'
import styles from './ImageSlide.module.sass'

const ImageSlide = () => {
  return (
    <div>
      <div className={styles.ImageSlide}>
        <div className={styles.ctlLeft}>
          <FontAwesomeIcon icon={faChevronLeft} />
        </div>
        <div className={styles.container}>
          <img src={Product} alt="product" className={styles.img}/>
        </div>
        <div  className={styles.ctlRight}>
          <FontAwesomeIcon icon={faChevronRight} />
        </div>     
      </div>
      <div className={styles.ProductDescription}>
        <ProductDescription />
      </div>
    </div>
  )
}

export default ImageSlide