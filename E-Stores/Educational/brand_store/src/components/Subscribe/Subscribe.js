import React from 'react';
import styles from './Subscribe.module.sass'
import avatar from './img/avatar.png'

const Subscribe = () => {
  return (
    <div className={styles.subscribe}>
      <div className={styles.subscribe__container}>
        <div className={styles.sliderBox}>
          <div className={styles.sliderContainer}>
            <img src={avatar} alt="avatar" className={styles.sliderContainer__img}/>
            <div className={styles.sliderContainer__textBox}>
              <p className={styles.sliderContainer__textBox__description}>
                “Vestibulum quis porttitor dui!
                Quisque viverra nunc mi, 
                a pulvinar purus condimentum a.
                Aliquam condimentum mattis neque sed pretium”
              </p>
              <p className={styles.sliderContainer__textBox__title}>Bin Burhan</p>
              <p className={styles.sliderContainer__textBox__subtitle}>Dhaka, Bd</p>
              <ul className={styles.sliderContainer__textBox__indicators}>
                <li></li>
                <li className={styles.indicator__active}></li>
                <li></li>
              </ul>
            </div>
          </div>
        </div>
        <div className={styles.subscribeBox}>
          <h2 className={styles.subscribeBox__title}>Subscribe</h2>
          <p className={styles.subscribeBox__subtitle}>FOR OUR NEWLETTER AND PROMOTION</p>
          <form className={styles.subscribeBox__form}>
            <input
              type="mail"
              placeholder="Enter Your Email"
              className={styles.subscribeBox__form__input}
            />
            <input 
              type="submit"
              value="Subscribe"
              className={styles.subscribeBox__form__submit}
            />
          </form>
        </div>
      </div>
    </div>
  )
}

export default Subscribe