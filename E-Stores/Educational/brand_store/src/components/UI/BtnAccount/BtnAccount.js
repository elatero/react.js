import React from 'react';
import styles from './BtnAccount.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown } from '@fortawesome/free-solid-svg-icons'

const BtnAccount = () => {
  return (
    <button className={styles.btn}>
      My Account
      <FontAwesomeIcon icon={faCaretDown} className={styles.btn__icon}/>
    </button>
  )
}

export default BtnAccount