import React, { useState } from 'react'
import styles from './Cart.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faStarHalfAlt, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import CartIcon from './img/cart_icon.png'
import ProductIcon_1 from './img/product_1.jpg'
import ProductIcon_2 from './img/product_2.jpg'
import {NavLink, withRouter} from 'react-router-dom'

const Cart = (props) => {

  const [listCart, setVisible] = useState(false)

  const [emptyCart, setCart] = useState(true)

  const cartList = () => {
    return (
      <div className={styles.boxProduct}>
        <div className={styles.product}>
          <img src={ProductIcon_1} alt="icon" className={styles.product__icon}/>
          <div className={styles.product__description}>
            <h2 className={styles.product__name}>Rebox Zane</h2>
            <div className={styles.product__starBox}>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStarHalfAlt} className={styles.product__star}/>
            </div>
            <div className={styles.product__priceBox}>
              <p>
                <span className={styles.product__count}>1</span>&nbsp; x &nbsp;<span className={styles.product__price}>$250</span>
              </p>
            </div>
          </div>
          <div className={styles.product__actionBox}>
            <button onClick={() => setCart(emptyCart)} className={styles.product__btnAction}>
              <FontAwesomeIcon icon={faTimesCircle} className={styles.product__btnAction__icon}/>
            </button>            
          </div>
        </div>
        <div className={styles.product}>
          <img src={ProductIcon_2} alt="icon" className={styles.product__icon}/>
          <div className={styles.product__description}>
            <h2 className={styles.product__name}>Rebox Zane</h2>
            <div className={styles.product__starBox}>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStar} className={styles.product__star}/>
              <FontAwesomeIcon icon={faStarHalfAlt} className={styles.product__star}/>
            </div>
            <div className={styles.product__priceBox}>
              <p>
                <span className={styles.product__count}>1</span>&nbsp; x &nbsp;<span className={styles.product__price}>$250</span>
              </p>
            </div>
          </div>
          <div className={styles.product__actionBox}>
            <button onClick={() => setCart(emptyCart)} className={styles.product__btnAction}>
              <FontAwesomeIcon icon={faTimesCircle} className={styles.product__btnAction__icon}/>
            </button> 
          </div>
        </div>
      </div>
    )
  }

  const cartBox = () => {
    return (
      <div className={styles.listCartDrop}>
        <div>
          <div className={styles.cartContent}>
            { emptyCart ? cartList() : <p className={styles.cartEmpty}>Cart Empty</p> }
          </div>
        </div>
        <div className={styles.cartBoxTotal}>
          <div className={styles.cartTotal}>
            <span>TOTAL</span>
            <span>$500.00</span>
          </div>
          <button
            className={styles.btnCart}
            onClick={() => props.history.push('/checkout')}
          >Checkout</button>
          <NavLink
            to="/cart"
            className={styles.btnCart}
          >Go to cart</NavLink>
        </div> 
      </div>
    )
  }

  const countProduct = () => {
    return (
      <div className={styles.count}>
        <span className={styles.count__number}>2</span>
      </div>
    )
  }
  return (
    <div className={styles.cart}>
      <button className={styles.btnCartIcon} onClick={() => setVisible(!listCart)}>
        <img src={CartIcon} alt="cart"/>
      </button>
      { listCart && cartBox() }
      { emptyCart && countProduct() }
    </div>
  )
}

export default withRouter(Cart)