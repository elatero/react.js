import React from 'react'
import { Link } from 'react-router-dom'

import styles from './Logo.module.sass'
import logo from './img/logo.png'

const Logo = () => {
  return (
    <Link to="/" >
     <div className={styles.logo}>
        <img src={logo} alt="logo" className={styles.logo__img}/>
        <h2 className={styles.logo__name}>BRAN<span className={styles.logo__D}>D</span></h2>
      </div>
    </Link>
  )
}

export default Logo