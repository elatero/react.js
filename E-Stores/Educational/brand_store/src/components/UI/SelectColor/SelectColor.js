import React, { useState} from 'react'
import styles from './SelectColor.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronDown } from '@fortawesome/free-solid-svg-icons'
import classNames from 'classnames/bind'
 
const SelectColor = (props) => {

  const [option, setVisible] = useState(false)
  const [color, setColor] = useState(styles.red)
  const [text, setText] = useState('Red')

  let cx = classNames.bind(styles)
  
  const handleOption = (event) => {    
    let item = event.currentTarget.id
    setText(props.Option[item].text)
    setColor(props.Option[item].color)
    setVisible(!option)
  }

  const Option = (props) => {
    return (
      <div className={styles.option}>
        <ul className={styles.list}>
          {props.Option.map((el, index) => {
            let color = el.color
            return (
              <li className={styles.item} key={index} id={index} onClick={handleOption}>
                <div className={cx(styles.icon, color)}></div>
                <div className={styles.text}>{el.text}</div>
              </li>
            )
          })}
        </ul>
      </div>  
    )
  }

  return (
   <div className={styles.box}>
      <div className={styles.SelectColor} onClick={() => setVisible(!option)}>
        <div className={styles.item} id='selectCurrent'>
          <div className={cx(styles.icon, color)}></div>
          <div className={styles.text} id='value'>{text}</div>
        </div>
        <div className={styles.btn} >
          <FontAwesomeIcon icon={faChevronDown} />
        </div>
      </div>
      { option && Option(props)}
    </div>    
  )
}

export default SelectColor