import React, { useState } from 'react'
import styles from './SearchForm.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCaretDown, faSearch } from '@fortawesome/free-solid-svg-icons'

const SearchForm = () => {

  const [list, setVisible] = useState(false)

  const showList  = () => {

    
    return (
      <div className={styles.dropList}>
        <nav>
          <h1 className={styles.title}>WOMEN</h1>
          <ul className={styles.list}>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Dresses</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Tops</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Sweaters/Knits</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Jackets/Coats</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Blazers</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Denim</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Leggings/Pants</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Skirts/Shorts</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Accessories</a>
            </li>
          </ul>
        </nav>
        <nav>
          <h1 className={styles.title}>MAN</h1>
          <ul className={styles.list}>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Tees/Tank tops</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Shirts/Polos</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Sweaters</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Sweatshirts/Hoodies</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Blazers</a>
            </li>
            <li className={styles.list__item}>
              <a href="##" className={styles.list__link}>Jackets/vests</a>
            </li>            
          </ul>
        </nav>
      </div>
    )
  }

  return (
    <div className={styles.formSearch}>
      <button 
        className={styles.btnBrowse}
        onClick={() => setVisible(!list)}
      >
        Browse
        <FontAwesomeIcon icon={faCaretDown} className={styles.btnBrowse__icon}/>
      </button>            
        { list && showList() }      
      <input type="text" placeholder="Search for Item..." className={styles.inputSearch}/>
      <button className={styles.btnSearch}>
        <FontAwesomeIcon icon={faSearch} className={styles.btnSearch__icon}/>
      </button>
    </div>
  )
}

export default SearchForm