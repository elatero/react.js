import React from 'react'
import styles from './Breadcrumbs.module.sass'
import { Link } from 'react-router-dom';

const Breadcrumbs = () => {
  return (
    <div className={styles.Breadcrumbs}>
      <div className={styles.container}>
        <h1 className={styles.namePage}>New Arrivals</h1>
        <nav className={styles.navigation}>
          <ul className={styles.list}>
            <li className={styles.list__item}>
              <Link to="/" className={styles.list__item__link}>Home</Link>
            </li> 
            <li>/</li>
            <li className={styles.list__item}>
              <Link to="/Man" className={styles.list__item__link}>Men</Link>
            </li>
            <li>/</li>
            <li className={styles.list__item}>
              <Link to="/newArrivals" className={styles.list__item__link_active}>New Arrivals</Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}

export default Breadcrumbs