import React from 'react';
import Breadcrumbs from '../../components/Breadcrumbs';
import FilterName from '../../components/Filtreds/FilterName';
import TrendingNow from '../../components/Filtreds/TrendingNow';
import Size from '../../components/Filtreds/Size';
import Price from '../../components/Filtreds/Price';
import Sort from '../../components/Filtreds/Sort';
import Product from '../../components/Product/Goods/Product';
import NavigationPage from './NavigationPage';
import Feature from './Feature';

import styles from './ProductsPage.module.sass'
import { img_1, img_2, img_3, img_4, img_5, img_6, img_7, img_8, img_9 } from '../MainPage/img/product/men'

const ProductsPage = () => {

  const products = [
    {
      id: 1,
      img: img_1,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 2,
      img: img_2,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 3,
      img: img_3,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 4,
      img: img_4,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 5,
      img: img_5,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 6,
      img: img_6,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 7,
      img: img_7,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 8,
      img: img_8,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 9,
      img: img_9,
      name: 'Mango People T-shirt',
      price: 52
    }
  ]

  const buyProduct = (event) => {   
    console.log(event.currentTarget.id)
  }

  return (
    <div className={styles.ProductsPage}>
      <Breadcrumbs />
      <div className={styles.main}>
        <div className={styles.container}>
          <div className={styles.column}>
            <FilterName />
          </div>
          <div className={styles.column}>
            <div className={styles.filtres}>
              <TrendingNow />
              <Size />
              <Price />
            </div>
            <div className={styles.filtres}>
              <Sort />
            </div>
            <div className={styles.products}>
              {products.map((item, index) => {
                  return (
                    <Product 
                      key = { index }
                      img = { item.img }
                      name = { item.name }
                      price = { item.price }
                      id = { item.id }
                      buy = { buyProduct }
                    />
                  )
                })
              }
            </div>
            <div className={styles.products}>
              <NavigationPage />
            </div>
          </div>
        </div>
      </div>
      <Feature />
    </div>
  )
}

export default ProductsPage