import React from 'react'
import styles from './Feature.module.sass'
import dilivery from './img/dilivery.png'
import sale from './img/sale.png'
import quality from './img/quality.png'

const Feature = () => {
  return (
    <div className={styles.Feature}>
      <div className={styles.container}>
        <div className={styles.column}>
          <img src={dilivery} alt="dilivery" className={styles.img}/>
          <h2 className={styles.title}>Free Delivery</h2>
          <p className={styles.description}>
            Worldwide delivery on all.
            Authorit tively morph next-generation
            innov tion with extensive models.
          </p>
        </div>
        <div className={styles.column}>
          <img src={sale} alt="sale" className={styles.img}/>
          <h2 className={styles.title}>Sales & discounts</h2>
          <p className={styles.description}>
            Worldwide delivery on all.
            Authorit tively morph next-generation
            innov tion with extensive models.
          </p>
        </div>
        <div className={styles.column}>
          <img src={quality} alt="quality" className={styles.img}/>
          <h2 className={styles.title}>Quality assurance</h2>
          <p className={styles.description}>
            Worldwide delivery on all.
            Authorit tively morph next-generation
            innov tion with extensive models.
          </p>
        </div>
      </div>
    </div>
  )
}

export default Feature