import React from 'react'
import styles from './NavigationPage.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faChevronLeft, faChevronRight} from '@fortawesome/free-solid-svg-icons'

const NavigationPage = () => {

  const cls = [
    styles.numberPage_link,
    styles.numberPage_link_active
  ]

  return (
    <div className={styles.NavigationPage}>
      <nav className={styles.navigation}>
        <ul className={styles.listPage}>
          <li className={styles.controller}>
            <a href="##" className={styles.controller_link}>
              <FontAwesomeIcon icon={faChevronLeft}/>
            </a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={cls.join(' ')}>1</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>2</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>3</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>4</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>5</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>6</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>.....</a>
          </li>
          <li className={styles.numberPage}>
            <a href="##" className={styles.numberPage_link}>20</a>
          </li>
          <li className={styles.controller}>
            <a href="##" className={styles.controller_link}>
              <FontAwesomeIcon icon={faChevronRight}/>
            </a>
          </li>
        </ul>
      </nav>
      <button className={styles.viewAllBtn}>View All</button>
    </div>
  )
}

export default NavigationPage
