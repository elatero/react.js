import React, { useState } from 'react'
import styles from './Accordion.module.sass'
import { Link } from 'react-router-dom'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'

const Accordion = () => {

  const [shipping, setShipping] = useState(true)
  const [billing, setBilling] = useState(false)
  const [shippingInfo, setShippingInfo] = useState(false)
  const [shippingMethod, setShippingMethod] = useState(false)
  const [paymentMethod, setPaymentMethod] = useState(false)
  const [orderReviev, setOrderReviev] = useState(false)

  return (
    <div className={styles.Accordion}>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setShipping(!shipping)}>01.<span>Shipping Adress</span></h2>
        {
          shipping &&
          <div className={styles.main}>
            <div className={styles.col}>
              <h3 className={styles.caption}>Check as a guest or register</h3>
              <p className={styles.subtitle}>Register with us for future convenience</p>
              <div className={styles.boxRadio}>
                <input type="radio" name="guest" id="guest"/>
                <label htmlFor="guest">checkout as guest</label>
              </div>
              <div className={styles.boxRadio}>
                <input type="radio" name="register" id="register"/>
                <label htmlFor="register">register</label>
              </div>
              <h3  className={styles.caption}>register and save time!</h3>
              <div className={styles.boxLinkTop}>
                <Link to="#" className={styles.linkRegister}>Register with us for future convenience</Link>
              </div>
              <div className={styles.boxLink}>
                <Link to="#"><FontAwesomeIcon icon={faAngleDoubleRight}/>Fast and easy checkout</Link>
              </div>
              <div className={styles.boxLink}>
                <Link to="#"><FontAwesomeIcon icon={faAngleDoubleRight}/>Easy access to your order history and status</Link>
              </div>
              <button className={styles.btn}>Continue</button>
            </div>
            <div className={styles.col}>
              <h3 className={styles.caption}>Already registed?</h3>
              <p className={styles.subtitle}>Please log in below</p>
              <form action="#" className={styles.form}>
                <div className={styles.boxEmail}>
                  <label htmlFor="email" className={styles.labelEmail}>EMAIL ADDRESS<span>*</span></label>
                  <input type="email" name="email" id="email" className={styles.inputEmail}/>
                </div>
                <div className={styles.boxPassword}>
                  <label htmlFor="password" className={styles.labelPassword}>PASSWORD<span>*</span></label>
                  <input type="password" name="password" id="password" className={styles.inputPassword}/>
                </div>
                <p className={styles.required}>* Required Fileds</p>
              </form>
              <button className={styles.btn}>Log in</button>
              <Link to="#" className={styles.forgotLink}>Forgot Password ?</Link>
            </div>
          </div>
        }        
        <div className={styles.line}></div>
      </div>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setBilling(!billing)}>02.<span>BILLING INFORMATION</span></h2>
          { billing &&  <div className={styles.main}></div> }        
        <div className={styles.line}></div>
      </div>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setShippingInfo(!shippingInfo)}>03.<span>SHIPPING INFORMATION</span></h2>
          { shippingInfo &&  <div className={styles.main}></div> }
        <div className={styles.line}></div>
      </div>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setShippingMethod(!shippingMethod)}>04.<span>SHIPPING METHOD</span></h2>
          { shippingMethod &&  <div className={styles.main}></div> }
        <div className={styles.line}></div>
      </div>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setPaymentMethod(!paymentMethod)}>05.<span>PAYMENT METHOD</span></h2>
          { paymentMethod &&  <div className={styles.main}></div> }
        <div className={styles.line}></div>
      </div>
      <div className={styles.container}>
        <h2 className={styles.title} onClick={() => setOrderReviev(!orderReviev)}>06.<span>ORDER REVIEW</span></h2>
        { orderReviev &&  <div className={styles.main}></div> }        
        <div className={styles.line}></div>
      </div>
    </div>
  )
}

export default Accordion