import React from 'react'
import styles from './Checkout.module.sass'
import Header from '../../components/Header'
import Navigation from '../../components/Navigation'
import Breadcrumbs from '../../components/Breadcrumbs'
import Accordion from './Accordion'
import Subscribe from '../../components/Subscribe'
import Footer from '../../components/Footer'
import Copyright from '../../components/Copyright'

const Checkout = () => {
  return (
    <div className={styles.Checkout}>
      <Header />
      <Navigation />
      <Breadcrumbs />
      <div className={styles.container}>
        <Accordion />
      </div>
      <Subscribe />
      <Footer />
      <Copyright />
    </div>
  )
}

export default Checkout