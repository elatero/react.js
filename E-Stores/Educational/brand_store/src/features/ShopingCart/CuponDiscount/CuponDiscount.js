import React from 'react'
import styles from './CuponDiscount.module.sass'

const  CuponDiscount = () => {
  return (
    <div className={styles.CuponDiscount}>
      <h2 className={styles.title}>coupon  discount</h2>
      <p className={styles.description}>Enter your coupon code if you have one</p>
      <input type="text" className={styles.input} placeholder="State"/>
      <button className={styles.btn}>Apply coupon</button>
    </div>
  )
}

export default CuponDiscount