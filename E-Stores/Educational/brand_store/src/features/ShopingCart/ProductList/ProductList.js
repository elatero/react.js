import React from 'react'
import styles from './ProductList.module.sass'
import ProductItem from './ProductItem'
import { img_1, img_2, img_3} from './img'

const ProductList = () => {

  const products = [
    {
      id: 1,
      img: img_1,
      name: 'Mango People T-shirt',
      price: 150,
      color: 'Red',
      size: 'XLL',
      shipping: 'Free'
    },
    {
      id: 2,
      img: img_2,
      name: 'Mango People T-shirt',
      price: 150,
      color: 'Red',
      size: 'XLL',
      shipping: 'Free'
    },
    {
      id: 3,
      img: img_3,
      name: 'Mango People T-shirt',
      price: 150,
      color: 'Red',
      size: 'XLL',
      shipping: 'Free'
    }
  ]

  return (
    <div className={styles.ProductList}>
      <div className={styles.container}>
        <div className={styles.caption}>
          <h5 className={styles.title}>Product Details</h5>
          <h5 className={styles.title}>unite Price</h5>
          <h5 className={styles.title}>Quantity</h5>
          <h5 className={styles.title}>shipping</h5>
          <h5 className={styles.title}>Subtotal</h5>
          <h5 className={styles.title}>ACTION</h5>
        </div>
        <div className={styles.list}>
          {
            products.map((item, index) => {
              return (
                <ProductItem 
                  key = {index}
                  id = {item.id}
                  img = {item.img}
                  name = {item.name}
                  color = {item.color}
                  size = {item.size}
                  price = {item.price}
                  shipping = {item.shipping}
                />
              )
            })
          }          
        </div>
      </div>
    </div>
  )
}

export default ProductList