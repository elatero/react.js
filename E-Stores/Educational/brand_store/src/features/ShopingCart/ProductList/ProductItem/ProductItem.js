import React from 'react'
import styles from './ProductItem.module.sass'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons'

const ProductItem = (props) => {
  return (
    <div className={styles.ProductItem}>
      <div className={styles.productDetails}>
        <img src={props.img} className={styles.img} alt={props.name}/>
        <div className={styles.description}>
          <h2 className={styles.name}>{props.name}</h2>
          <p className={styles.color}>Color:<span>{props.color}</span></p>
          <p className={styles.size}>Size:<span>{props.size}</span></p>
        </div>
      </div>
      <div className={styles.unitePrice}>
        <p className={styles.price}>${props.price}</p>
      </div>
      <div className={styles.quantity}>
        <input type="number" placeholder="1"/>
      </div>
      <div className={styles.shipping}>
        {props.shipping}
      </div>
      <div className={styles.subtotal}>
        ${props.price}
      </div>
      <div className={styles.action}>
        <FontAwesomeIcon icon={faTimesCircle} />
      </div>
    </div>
  )
}

export default ProductItem
