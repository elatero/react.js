import React from 'react';
import styles from './ShopingCart.module.sass';
import Breadcrumbs from '../../components/Breadcrumbs';
import ProductList from './ProductList';
import ShippingAddress from './ShippingAddress';
import CuponDiscount from './CuponDiscount';
import Total from './Total';

const ShopingCart = () => {
  return (
    <div className={styles.ShopingCart}>
      <Breadcrumbs />
      <ProductList />
      <div className={styles.container}>
        <div className={styles.btnContainer}>
          <button className={styles.clearBtn}>CLEAR SHOPPING CART</button>
          <button className={styles.countinueBtn}>CONTINUE sHOPPING</button>
        </div>
        <div className={styles.totalContainer}>
          <ShippingAddress />
          <CuponDiscount />
          <Total />
        </div>
      </div>
    </div>
  )
}

export default ShopingCart