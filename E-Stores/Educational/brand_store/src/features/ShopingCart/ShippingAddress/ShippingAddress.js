import React from 'react'
import styles from './ShippingAddress.module.sass'

const ShippingAddress = () => {
  return (
    <div className={styles.ShippingAddress}>
      <h2 className={styles.title}>Shipping Adress</h2>
      <select name="country" id="country" className={styles.select}>
        <option value="Bangladesh">Bangladesh</option>
      </select>
      <input type="text" className={styles.input} placeholder="State"/>
      <input type="text" className={styles.input} placeholder="Postcode / Zip"/>
      <button className={styles.btn}>get a quote</button>
    </div>
  )
}

export default ShippingAddress