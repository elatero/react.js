import React from 'react';
import styles from './Total.module.sass';
import { NavLink } from 'react-router-dom';

const Total = () => {
  return (
    <div className={styles.Total}>
      <p className={styles.subTotal}>Sub total<span>$900</span></p>
      <p className={styles.grandTotal}>Grand total<span>$900</span></p>
      <div className={styles.line}></div>
      <NavLink
        to="/checkout"
        className={styles.btn}
      >proceed to checkout</NavLink>
    </div>
  )
}

export default Total