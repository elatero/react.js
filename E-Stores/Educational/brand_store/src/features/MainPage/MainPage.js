import React from 'react';
import styles from './MainPage.module.sass';
import Header from '../../components/Header';
import Navigation from '../../components/Navigation';
import Subscribe from '../../components/Subscribe';
import Footer from '../../components/Footer';
import Copyright from '../../components/Copyright';
import {Switch, Route} from 'react-router-dom';

import { img_1, img_2, img_3, img_4, img_5, img_6, img_7, img_8 } from './img/product';
import HomePage from '../HomePage';
import ProductsPage from '../ProductsPage';
import SinglePage from '../SinglPage';
import ShopingCart from '../ShopingCart';
import Checkout from '../Checkout';

const MainPage = () => {

  const products = [
    {
      id: 1,
      img: img_1,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 2,
      img: img_2,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 3,
      img: img_3,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 4,
      img: img_4,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 5,
      img: img_5,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 6,
      img: img_6,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 7,
      img: img_7,
      name: 'Mango People T-shirt',
      price: 52
    },
    {
      id: 8,
      img: img_8,
      name: 'Mango People T-shirt',
      price: 52
    }
  ]

  const handlerBuyProduct = (e) => {
    console.log(e.target.id)
  }

  const Home = () => <HomePage
    products = {products}
    handlerBuyProduct = {handlerBuyProduct}
  />
 
  return (
    <div className={styles.MainPage}>
      <Header/>
      <Navigation/>
      <Switch>
        <Route path="/" exact component={Home}/>
        <Route path="/men" component={ProductsPage}/>
        <Route path="/singlePage" component={SinglePage}/>
        <Route path="/cart" component={ShopingCart}/>
        <Checkout path="/checkout" component={Checkout} />
      </Switch>
      <Subscribe/>
      <Footer/>
      <Copyright/>
    </div>
  )
}

export default MainPage;