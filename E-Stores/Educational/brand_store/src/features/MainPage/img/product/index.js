import prod_1 from './product_1.jpg'
import prod_2 from './product_2.jpg'
import prod_3 from './product_3.jpg'
import prod_4 from './product_4.jpg'
import prod_5 from './product_5.jpg'
import prod_6 from './product_6.jpg'
import prod_7 from './product_7.jpg'
import prod_8 from './product_8.jpg'

export const img_1 = prod_1
export const img_2 = prod_2
export const img_3 = prod_3
export const img_4 = prod_4
export const img_5 = prod_5
export const img_6 = prod_6
export const img_7 = prod_7
export const img_8 = prod_8