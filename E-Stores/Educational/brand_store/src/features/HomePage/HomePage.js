import React from 'react'
import styles from './HomePage.module.sass'
import Product from './../../components/Product/Goods/index';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faLongArrowAltRight } from '@fortawesome/free-solid-svg-icons';
import Offer from './../../components/Offer';

const HomePage = (props) => {

  return (
    <div>
      <div className={styles.slider}>
        <div className={styles.sliderContainer}>
          <div className={styles.sliderDescription}>
            <div className={styles.sliderDescription__line}></div>
            <div className={styles.sliderDescription__box}>
              <h3 className={styles.sliderDescription__box__title}>THE BRAND</h3>
              <h4 className={styles.sliderDescription__box__subtitle}>OF LUXERIOUS <span>FASHION</span></h4>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.promotion}>
        <div className={styles.promotionContainer}>
          <div className={styles.promotionContainer__column}>
            <div className={styles.promotionMen}>
              <div className={styles.promotionText}>
                <h2 className={styles.promotionText__title}>HOT DEAL</h2>
                <h1 className={styles.promotionText__subtitle}>FOR MEN</h1>
              </div>
            </div>
            <div className={styles.promotionAccesories}>
              <div className={styles.promotionText}>
                <h2 className={styles.promotionText__title}>LUXIROUS & TRENDY</h2>
                <h1 className={styles.promotionText__subtitle}>ACCESORIES</h1>
              </div>
            </div>
          </div>
          <div className={styles.promotionContainer__column}>
            <div className={styles.promotionWomen}>
              <div className={styles.promotionText}>
                <h2 className={styles.promotionText__title}>30% offer</h2>
                <h1 className={styles.promotionText__subtitle}>women</h1>
              </div>
            </div>
            <div className={styles.promotionKids}>
              <div className={styles.promotionText}>
                <h2 className={styles.promotionText__title}>new arrivals</h2>
                <h1 className={styles.promotionText__subtitle}>FOR kids</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className={styles.feturedItems}>
        <div className={styles.feturedItemsContainer}>
          <h1 className={styles.feturedItems__title}>Fetured Items</h1>
          <p className={styles.feturedItems__text}>
            Shop for items based on what we featured in this week
          </p>
        </div>
      </div>
      <div className={styles.products}>
        <div className={styles.products__container}>
          {props.products.map((item, index) => {
            return (
              <Product
                key = { index }
                img = { item.img }
                name = { item.name }
                price = { item.price }
                id = { item.id }
                buy = { props.buyProduct }
              />
            )
          })}
        </div>
      </div>
      <div className={styles.browseBtn}>
        <div className={styles.browseBtn__container}>
          <button className={styles.btnBrowseAllProduct}>
            Browse All Product
            <FontAwesomeIcon icon={faLongArrowAltRight} className={styles.btnBrowseAllProduct__img}/>
          </button>
        </div>
      </div>
      <Offer />
    </div>
  )
}

export default HomePage