import React from 'react'
import Breadcrumbs from '../../components/Breadcrumbs'
import ImageSlide from '../../components/ImageSlide'
import Product from '../../components/Product/Goods'
import { img_1, img_2, img_3, img_4} from '../MainPage/img/product/women'
import styles from './SinglePage.module.sass'

const SinglePage = () => {

  const products = [
    {
      id: 1,
      img: img_1,
      name: 'BLAZE LEGGINGS',
      price: 52
    },
    {
      id: 2,
      img: img_2,
      name: 'ALEXA SWEATER',
      price: 52
    },
    {
      id: 3,
      img: img_3,
      name: 'AGNES TOP   ',
      price: 52
    },
    {
      id: 4,
      img: img_4,
      name: 'SYLVA SWEATER',
      price: 52
    }
  ]

  const buyProduct = (event) => {   
    console.log(event.currentTarget.id)
  }

  return (
    <div className={styles.SinglPage}>
      <Breadcrumbs />
      <div>
        <ImageSlide />
      </div>
      <div className={styles.products}>
        <div className={styles.container}>
          <h2 className={styles.title}>you may like also</h2>
          <div className={styles.listProduct}>
            {products.map((item, index) => {
                return (
                  <Product 
                    key = { index }
                    img = { item.img }
                    name = { item.name }
                    price = { item.price }
                    id = { item.id }
                    buy = { buyProduct }
                  />
                )
              })
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default SinglePage